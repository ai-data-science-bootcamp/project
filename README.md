# AI-DS-BootCamp-Capstone-Project

Bootcamp capstone bonus project


## Motivation
This project is to challenge you as a bootcamper to push yourself after the bootcamp, and show-off your skills in AI & Data Science, <br/>
specifically in 3 sub-fields, NLP, Time-series analysis, and Computer Vision.

## Business Context 
An Egyptian Food & Beverage chain named Egyptian Foodies (EF) has approached Lyra and has provided their datasets from their 3 different data silos: Point of Sale (POS), Social Media & Cameras. Our goal as a team is to use our AI & Data Science skills to output the maximum business value to client using the dataset provided by demonstrating to the client the applications that could be used to improve profitabily. 

Lyra will assess the best teams by measuring on use of concepts taught at the bootcamp, innovation & most valuable applications. (To be discussed)

## Deadline of Submissions
29 Jan 2020 , 11:59 PM - No exceptions.

## How to  submit
Open a merge request with your code 

- Gitlab and Git tutorial : https://gist.github.com/m-kyle/fb0f3e9edc369adfcac7
- Alternative tutorial : https://indico.him.uni-mainz.de/event/37/material/slides/0.pdf

## What to submit 
- Jupyter Notebook with your work
- Python code for training and testing
- Model in pkl or h5 format
- Instructions for how to run it
- Presentation demonstrating the value/output to the client 

## Deadline 

Team submission: 29 Jan 2020 , 11:59 PM - No exceptions.
Project submission: 3rd Feb 2020, 11:59 PM- No exceptions.

## Datasets 
1. Excel file (attached in repository) with sales of EF
2. Excel file (attached in repository) with reviews on EF food & beverage
3. Excel file (attached in repository) with images of EF people & food 



### Time-Series 
The sales sheet was extracted from their POS on a monthly basis starting January 2017 to December 2019 (3 years). The data was reported in a way that acccumulates the sum of the monthly slaes at the beginner day of every month. 


### NLP 
The review sheet was extracted from several social media platforms including the website, Facebook & Instagram. There are mainly 5 columns: 
1. Purchased Reviews (True/False: means that the person that did the review made a purchase or not at the stores)
2. Recommended Reviews (True/False: means that the person that did the review recommended the purchase or not at the stores)
3. Rating of Reviews (from 1-5 is the ranking score made by the reviewer with 5 being the highest and 1 being the lowest)
4. Full text reviews (This the full text review by the reviewer)
5. Title of review (This is the tike given by the reviewer to the review)


### Computer Vision 
The dataset are extracted snapshots of EF's cameras taking pictures of people eating & dishes at EF. Below are the main columns:
1. Gender of the people in the pictures either male of female.
2. The confidence level of which the male/female identification is accurate (from 0-1)
3. The age of the people in the picture. 
4. The confidence level of which the age category identification is accurate (from 0-1)
5. Whether a person or not is eating.
6. The confidence level of which we are sure that the person identification is accurate (from 0-1)
7. The image URL


Note: 
* > the above datasets are inspired by a real business case (dataset sources will be revealed after project submission)
* > you can ask Lyra any questions you want as they are representing the client. 
* > you can include all datasets and use more tools/data per your choice, and should know that all datasets could be included in the project.
* > we suggest you at least implement the minimum levels you were taught in the bootcamp, not limited to that you can aslo add further research and team expertise into consideration. 



